<?php

namespace jf\Collection;

/**
 * Colección para almacenar arrays.
 *
 * @extends ACollection<array>
 */
class Arrays extends ACollection
{
    /**
     * @inheritdoc
     */
    public function isItem(mixed $item) : bool
    {
        return is_array($item);
    }
}

<?php

namespace jf\Collection;

/**
 * Clase para las colecciones que no aceptan un parámetro en el constructor porque ellos le
 * inyectan la clase que gestionan.
 *
 * ```php
 * class Users extends ANamed
 * {
 *     public function __construct()
 *     {
 *         parent::__construct(User::class);
 *     }
 * }
 * ```
 *
 * @template T of object
 *
 * @extends Objects<T>
 */
abstract class ANamed extends Objects
{
    /**
     * @inheritdoc
     */
    public static function fromItems(array $items) : static
    {
        // Es responsabilidad de las clases que la usan no aceptar parámetros en el constructor.
        $collection = new static();
        if ($items)
        {
            $collection->addItems($items);
        }

        return $collection;
    }
}

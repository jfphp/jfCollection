<?php

namespace jf\Collection;

/**
 * Interfaz para los filtros de la colección.
 */
interface IFilter
{
    /**
     * Filtra la colección.
     *
     * @param ACollection $collection Colección a filtrar.
     *
     * @return ACollection
     */
    public function filter(ACollection $collection) : ACollection;
}

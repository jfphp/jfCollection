<?php

namespace jf\Collection;

/**
 * Colección para almacenar elementos de cualquier tipo.
 *
 * @extends ACollection<mixed>
 */
class Any extends ACollection
{
    /**
     * @inheritdoc
     */
    public function isItem(mixed $item) : bool
    {
        return TRUE;
    }
}

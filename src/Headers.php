<?php

namespace jf\Collection;

/**
 * Colección simple de encabezados HTTP.
 *
 * @extends ACollection<string>
 */
class Headers extends Strings
{
    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $headers = [];
        foreach ($this->_items as $header => $value)
        {
            if (is_array($value))
            {
                $value = implode('; ', $value);
            }
            $headers[] = "$header: $value";
        }

        return implode("\r\n", $headers);
    }

    /**
     * Agrega un listado de encabezados.
     *
     * @param string|string[] $headers Listado de encabezados a analizar.
     *
     * @return static
     */
    public function addHeaders(array|string $headers) : static
    {
        if ($headers && is_string($headers))
        {
            $headers = explode("\r\n", trim($headers));
        }
        if ($headers)
        {
            foreach ($headers as $header)
            {
                if (str_contains($header, ':'))
                {
                    [ $header, $value ] = preg_split('/:\s*/', $header, 2);
                    $this->set($header, $value);
                }
            }
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function _buildValue(mixed $item) : string
    {
        return trim(Strings::transform($item));
    }

    /**
     * Construye una instancia y la inicializa con los valores de $_SERVER.
     *
     * @return static
     */
    public static function fromServer() : static
    {
        $headers = new static();
        foreach ($_SERVER as $key => $value)
        {
            $header = '';
            if ($key === 'CONTENT_TYPE')
            {
                $header = $key;
            }
            elseif (str_starts_with($key, 'HTTP_'))
            {
                $header = str_replace('_', '-', substr($key, 5));
            }
            if ($header)
            {
                $headers->set($header, $value);
            }
        }

        return $headers;
    }

    /**
     * @inheritdoc
     */
    public function set(mixed $key, mixed $value) : static
    {
        if ($this->isItem($value))
        {
            $value = $this->_buildValue($value);
            if ($value)
            {
                $key     = $this->_buildKey($key);
                $current = $this->getItem($key);
                if ($current !== NULL)
                {
                    $value = is_array($current)
                        ? [ ...$current, $value ]
                        : [ $current, $value ];
                }
                $this->_items[ $key ] = $value;
            }
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public static function transform(mixed $value) : string
    {
        return ucwords(strtolower(Strings::transform($value)), '-');
    }
}

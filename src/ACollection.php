<?php

namespace jf\Collection;

use ArrayAccess;
use BackedEnum;
use Countable;
use Iterator;
use jf\Base\AAssign;
use jf\Base\Array\TArray;
use Stringable;
use UnitEnum;

/**
 * Clase base para las colecciones.
 *
 * @template T of array|scalar|object
 *
 * @implements ArrayAccess<T>
 * @implements Iterator<T>
 */
abstract class ACollection extends AAssign implements ArrayAccess, Countable, Iterator
{
    use TArray;

    /**
     * Construye el listado de elementos especificado y lo agrega al listado.
     *
     * @param array<array<array<string,scalar>|T>> $items Listado de elementos a construir.
     *
     * @return static
     */
    public function addItems(array $items) : static
    {
        foreach ($items as $k => $v)
        {
            $this->set($k, $v);
        }

        return $this;
    }

    /**
     * Filtra los elementos de la colección para los que la función devuelve un valor verdadero y
     * devuelve los valores como un array.
     *
     * @param callable(T,int|string):bool|null $fn Función a ejecutar para cada uno de los elementos.
     *                                             Recibe como primer parámetro el valor y como segundo la clave.
     *
     * @return array<T>
     */
    public function arrayFilter(?callable $fn = NULL) : array
    {
        if (!$fn)
        {
            $fn = static fn($value) => (bool) $value;
        }
        $items = [];
        foreach ($this->getItems() as $key => $value)
        {
            if ($fn($value, $key))
            {
                $items[ $key ] = $value;
            }
        }

        return $items;
    }

    /**
     * Devuelve un array con los valores que son iguales al especificado.
     *
     * @param T $value Valor a buscar.
     *
     * @return array<T>
     */
    public function arrayFind(bool|float|int|string|null $value) : array
    {
        return $this->arrayFilter(fn($item) => $item === $value);
    }

    /**
     * Aplica la función a los elementos de la colección y retorna un array con el resultado de cada llamada.
     *
     * @param callable $fn Función a ejecutar para cada uno de los elementos.
     *
     * @return array<T>
     */
    public function arrayMap(callable $fn) : array
    {
        return array_map($fn, $this->getItems());
    }

    /**
     * Construye la clave del elemento.
     *
     * @param mixed $item Elemento a analizar para construir su clave.
     *
     * @return int|string|null
     */
    protected function _buildKey(mixed $item) : int|string|null
    {
        return $item instanceof IItem
            ? $item->getCollectionKey()
            : NULL;
    }

    /**
     * Crea una nueva instancia de la colección pero con los elementos especificados en vez de los actuales.
     *
     * @param array<array<string,scalar>|T> $items Elementos a agregar a la colección.
     *
     * @return static
     */
    public function clone(array $items = []) : static
    {
        $clone = clone $this;
        $clone->clear();
        if ($items)
        {
            $clone->addItems($items);
        }

        return $clone;
    }

    /**
     * Filtra los elementos de la colección para los que la función devuelve un valor verdadero y
     * devuelve los valores como otra colección.
     *
     * @param IFilter|callable(T,int|string):bool|NULL $fn Función a ejecutar para cada uno de los elementos.
     *                                                     Recibe como primer parámetro el valor y como segundo la clave.
     *
     * @return static
     */
    public function filter(IFilter|callable|null $fn = NULL) : static
    {
        return $fn instanceof IFilter
            ? $fn->filter($this)
            : $this->clone($this->arrayFilter($fn));
    }

    /**
     * Devuelve una colección con los valores que son iguales al especificado.
     *
     * @param T $value Valor a buscar.
     *
     * @return static
     */
    public function find(mixed $value) : static
    {
        return $this->clone($this->arrayFind($value));
    }

    /**
     * Devuelve una instancia e inicializa los elementos.
     *
     * @param array<array<string,scalar>|T> $items Elementos a agregar a la colección.
     *
     * @return static
     */
    public static function fromItems(array $items) : static
    {
        $collection = new static();
        if ($items)
        {
            $collection->addItems($items);
        }

        return $collection;
    }

    /**
     * Agrupa los elementos por el valor de una propiedad.
     *
     * @param string $property Nombre de la propiedad a usar para agrupar el resultado.
     *
     * @return array
     */
    public function groupBy(string $property) : array
    {
        $items = [];
        $this->each(
            static function (object $item, $key) use (&$items, $property)
            {
                $group = $item->$property ?? '';
                if ($group instanceof BackedEnum)
                {
                    $group = $group->value;
                }
                elseif ($group instanceof UnitEnum)
                {
                    $group = $group->name;
                }
                elseif ($group instanceof Stringable)
                {
                    $group = (string) $group;
                }
                $items[ $group ][ $key ] = $item;
            }
        );

        return $items;
    }

    /**
     * Verifica si el elemento es del mismo tipo que la de los elementos gestionados por la colección.
     *
     * @param mixed $item Elemento a verificar.
     *
     * @return bool
     */
    abstract public function isItem(mixed $item) : bool;

    /**
     * @inheritdoc
     */
    public function join(string $separator = ',') : string
    {
        return implode(
            $separator,
            array_filter(
                array_map(
                    fn($item) => is_scalar($item) || $item instanceof Stringable ? strval($item) : NULL,
                    $this->getItems()
                )
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize() : array
    {
        return static::serializeIterable($this->getItems());
    }

    /**
     * Aplica la función a los elementos de la colección y retorna una nueva colección con el resultado de cada llamada.
     *
     * @param callable $fn Función a ejecutar para cada uno de los elementos.
     *
     * @return static
     */
    public function map(callable $fn) : static
    {
        return $this->clone($this->arrayMap($fn));
    }

    /**
     * Agrega el contenido de la colección a la colección actual.
     *
     * @param ACollection|array $collection Elementos o colección que aportará los elementos a agregar.
     *
     * @return static
     */
    public function merge(ACollection|array $collection) : static
    {
        $this->addItems(
            is_array($collection)
                ? $collection
                : $collection->getItems()
        );

        return $this;
    }

    /**
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value) : void
    {
        if ($offset === NULL)
        {
            $this->push($value);
        }
        else
        {
            $this->set($offset, $value);
        }
    }

    /**
     * @inheritdoc
     */
    public function push(mixed $item) : static
    {
        if ($this->isItem($item))
        {
            $key = $this->_buildKey($item);
            if ($key === NULL)
            {
                $this->_items[] = $item;
            }
            else
            {
                $this->set($key, $item);
            }
        }

        return $this;
    }

    /**
     * Asigna el valor a la clave especificada.
     *
     * @param mixed $key   Clave a la cual asignar el valor.
     * @param mixed $value Valor de la clave.
     *
     * @return static
     */
    public function set(mixed $key, mixed $value) : static
    {
        if ($this->isItem($value))
        {
            if ($key === NULL)
            {
                $this->_items[] = $value;
            }
            else
            {
                if (!is_int($key))
                {
                    $key = $key instanceof IItem
                        ? $key->getCollectionKey()
                        : Strings::transform($key);
                }
                $this->_items[ $key ] = $value;
            }
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function unshift(mixed $item) : static
    {
        if ($this->isItem($item))
        {
            $key = $item instanceof IItem
                ? $item->getCollectionKey()
                : NULL;
            if ($key === NULL)
            {
                array_unshift($this->_items, $item);
            }
            elseif (isset($this->_items[ $key ]))
            {
                $this->set($key, $item);
            }
            else
            {
                $this->_items = [ $key => $item, ...$this->_items ];
            }
        }

        return $this;
    }
}

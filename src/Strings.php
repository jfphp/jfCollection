<?php

namespace jf\Collection;

use BackedEnum;
use JsonSerializable;
use Stringable;
use UnitEnum;

/**
 * Colección para almacenar textos.
 * Cualquier valor que trate de agregarse a la colección que no sea un texto se tratará de transformar en un texto.
 *
 * @extends ACollection<string>
 */
class Strings extends ACollection
{
    /**
     * @inheritdoc
     */
    protected function _buildKey(mixed $item) : string
    {
        return $item instanceof IItem
            ? $item->getCollectionKey()
            : static::transform($item);
    }

    /**
     * Construye el valor apropiado a almacenar en la colección a partir del valor especificado.
     *
     * @param mixed $item Valor a almacenar.
     *
     * @return string
     */
    protected function _buildValue(mixed $item) : string
    {
        return static::transform($item);
    }

    /**
     * @inheritdoc
     */
    public function isItem(mixed $item) : bool
    {
        // Aceptamos cualquier valor que sea soportado por el método `Strings::transform`.
        return is_scalar($item) || is_object($item) || is_resource($item);
    }

    /**
     * @inheritdoc
     */
    public function set(mixed $key, mixed $value) : static
    {
        if ($this->isItem($value))
        {
            $this->_items[ $this->_buildKey($key) ] = $this->_buildValue($value);
        }

        return $this;
    }

    /**
     * Transforma el valor en un texto.
     *
     * @param mixed $value Valor a transformar.
     *
     * @return string
     */
    public static function transform(mixed $value) : string
    {
        return is_string($value)
            ? $value
            : match (TRUE)
            {
                //@formatter:off
                $value instanceof BackedEnum        => (string) $value->value,
                $value instanceof UnitEnum          => $value->name,
                $value instanceof Stringable        => (string) $value,
                $value instanceof JsonSerializable  => json_encode($value),
                is_object($value)                   => $value::class,
                is_resource($value)                 => sprintf('Resource[%s#%s]', get_resource_type($value), get_resource_id($value)),
                default                             => (string) $value
                //@formatter:on
            };
    }
}

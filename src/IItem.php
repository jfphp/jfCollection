<?php

namespace jf\Collection;

/**
 * Interfaz para los elementos que se agregarán a la colección.
 */
interface IItem
{
    /**
     * Devuelve la clave a usar por el elemento.
     *
     * @return int|string|null
     */
    public function getCollectionKey() : int|string|null;
}

<?php

namespace jf\Collection;

/**
 * Colección para almacenar números tanto enteros como de coma flotante.
 *
 * @extends ACollection<float|int>
 */
class Numbers extends ACollection
{
    /**
     * Devuelve el promedio de los valores almacenados en la colección.
     *
     * @return float
     */
    public function avg() : float
    {
        $count = count($this);

        return $count
            ? $this->sum() / $count
            : 0.0;
    }

    /**
     * @inheritdoc
     */
    public function isItem(mixed $item) : bool
    {
        return is_float($item) || is_int($item);
    }

    /**
     * Devuelve la suma de todos los elementos de la colección.
     *
     * @return float|int
     */
    public function sum() : float|int
    {
        return array_sum($this->_items);
    }
}
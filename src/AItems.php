<?php

namespace jf\Collection;

/**
 * Clase para las colecciones que no aceptan un parámetro en el constructor
 * porque la clase de los elementos la almacenan en una constante.
 *
 * ```php
 * class Users extends AItems
 * {
 *     public const ITEM_CLASS = User::class;
 * }
 * ```
 *
 * @template T of object
 *
 * @extends Objects<T>
 */
abstract class AItems extends ANamed
{
    /**
     * Nombre de la clase de los elementos gestionados por la colección.
     */
    public const ITEM_CLASS = '';

    /**
     * Constructor de la clase.
     */
    public function __construct()
    {
        parent::__construct(static::ITEM_CLASS);
    }
}

<?php

namespace jf\Collection;

use jf\assert\php\BadMethodCallException;
use jf\assert\php\InvalidArgumentException;
use jf\assert\php\LogicException;
use jf\Base\IAssign;

/**
 * Clase para las colecciones que gestionan objetos que se construyen a partir
 * del nombre de una clase.
 *
 * @template T of object
 *
 * @extends ACollection<T>
 */
class Objects extends ACollection
{
    /**
     * Constructor de la clase.
     *
     * @param class-string<T> $classname Nombre de la clase de los elementos de la colección.
     */
    public function __construct(public readonly string $classname)
    {
        BadMethodCallException::notEmpty(
            $classname,
            dgettext('collection', 'Se requiere el nombre de la clase gestionada por la colección')
        );
        BadMethodCallException::classExists($classname, TRUE, dgettext('collection', 'La clase {0} no existe'));
    }

    /**
     * @inheritdoc
     */
    public function addItems(array $items) : static
    {
        array_map($this->push(...), $items);

        return $this;
    }

    /**
     * Verifica si la colección contiene algún elemento que contenga el valor
     * especificado en la propiedad indicada.
     *
     * @param string $property Propiedad en la que buscar el valor.
     * @param scalar $value    Valor a buscar.
     *
     * @return bool
     */
    public function containsBy(string $property, bool|float|int|string|null $value) : bool
    {
        return $this->some(fn(object $item) => ($item->$property ?? NULL) === $value);
    }

    /**
     * Crea un elemento y le asigna los valores.
     *
     * @param array<string,scalar> $values Valores a asignar al elemento creado.
     *
     * @return T
     */
    protected function createItem(array $values = []) : object
    {
        $classname = $this->classname;
        if (is_a($classname, IAssign::class, TRUE))
        {
            $item = $classname::fromArray($values);
        }
        else
        {
            $item = new $classname($values);
        }

        return $item;
    }

    /**
     * Devuelve los valores que contienen el valor especificado en la propiedad indicada.
     *
     * @param string $property Propiedad en la que buscar el valor.
     * @param scalar $value    Valor a buscar.
     *
     * @return static
     */
    public function findBy(string $property, bool|float|int|string|null $value) : static
    {
        return $this->filter(
            fn(object $item) => ($item->$property ?? NULL) === $value
        );
    }

    /**
     * @inheritdoc
     */
    public static function fromItems(array $items) : static
    {
        LogicException::notEmpty(
            $items,
            dgettext('collection', 'Se debe especificar al menos un elemento para determinar su clase')
        );
        $item = reset($items);
        InvalidArgumentException::isObject($item, dgettext('collection', 'Los elementos de la colección deben ser objetos'));
        $collection = new static(get_class($item));
        $collection->addItems($items);

        return $collection;
    }

    /**
     * @inheritdoc
     */
    public function isItem(mixed $item) : bool
    {
        return is_a($item, $this->classname);
    }

    /**
     * Retorna una lista con los valores en cada elemento de la propiedad especificada.
     *
     * @param string $property Propiedad con los valores a extraer.
     *
     * @return scalar[]
     */
    public function pluck(string $property) : array
    {
        return array_map(fn(object $item) => $item->$property ?? NULL, $this->_items);
    }

    /**
     * @inheritdoc
     */
    public function push(mixed $item) : static
    {
        return parent::push($this->isItem($item) ? $item : $this->createItem($item));
    }

    /**
     * @inheritdoc
     */
    public function set(mixed $key, mixed $value) : static
    {
        return parent::set($key, $this->isItem($value) ? $value : $this->createItem($value));
    }

    /**
     * Suma el valor almacenado en una propiedad de los elementos.
     *
     * @param string $property Nombre de la propiedad.
     *
     * @return float
     */
    public function sum(string $property) : float
    {
        $sum = 0.0;
        foreach ($this->_items as $item)
        {
            $value = $item->$property ?? NULL;
            if ($value && is_numeric($value))
            {
                $sum += $value;
            }
        }

        return $sum;
    }

    /**
     * @inheritdoc
     */
    public function unshift(mixed $item) : static
    {
        return parent::unshift($this->isItem($item) ? $item : $this->createItem($item));
    }
}

# jf/collection

Clases para gestionar diferentes tipos de colecciones.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/collection` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/collection
```

#### Dependencias

Cuando el proyecto es instalado, adicionalmente se instalan las siguientes dependencias:

| Paquete   | Versión |
|:----------|:--------|
| jf/assert | ^3.1    |
| jf/base   | ^4.0    |

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone https://www.gitlab.com/jfphp/jfCollection.git
cd jfCollection
composer install
```

## Archivos disponibles

### Clases

| Nombre                                           | Descripción                                                                                                                              |
|:-------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------|
| [jf\Collection\ACollection](src/ACollection.php) | Clase base para las colecciones.                                                                                                         |
| [jf\Collection\AItems](src/AItems.php)           | Clase para las colecciones que no aceptan un parámetro en el constructor porque la clase de los elementos la almacenan en una constante. |
| [jf\Collection\ANamed](src/ANamed.php)           | Clase para las colecciones que no aceptan un parámetro en el constructor porque ellos le inyectan la clase que gestionan.                |
| [jf\Collection\Any](src/Any.php)                 | Colección para almacenar elementos de cualquier tipo.                                                                                    |
| [jf\Collection\Arrays](src/Arrays.php)           | Colección para almacenar arrays.                                                                                                         |
| [jf\Collection\Headers](src/Headers.php)         | Colección simple de encabezados HTTP.                                                                                                    |
| [jf\Collection\Numbers](src/Numbers.php)         | Colección para almacenar números tanto enteros como de coma flotante.                                                                    |
| [jf\Collection\Objects](src/Objects.php)         | Clase para las colecciones que gestionan objetos que se construyen a partir del nombre de una clase.                                     |
| [jf\Collection\Strings](src/Strings.php)         | Colección para almacenar textos.                                                                                                         |

### Interfaces

| Nombre                                   | Descripción                                                  |
|:-----------------------------------------|:-------------------------------------------------------------|
| [jf\Collection\IFilter](src/IFilter.php) | Interfaz para los filtros de la colección.                   |
| [jf\Collection\IItem](src/IItem.php)     | Interfaz para los elementos que se agregarán a la colección. |

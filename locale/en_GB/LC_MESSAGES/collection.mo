��          D      l       �      �   0   �   A   �   >     /  R     �      �  >   �  3   �                          La clase {0} no existe Los elementos de la colección deben ser objetos Se debe especificar al menos un elemento para determinar su clase Se requiere el nombre de la clase gestionada por la colección Project-Id-Version: jf/collection 2.0.0
Report-Msgid-Bugs-To: Joaquín Fernández
PO-Revision-Date: 2024-03-04 21:25:19+0100
Last-Translator: Joaquín Fernández
Language-Team: Joaquín Fernández
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Class {0} not found Collection items must be objects At least one item must be specified to determine its classname The classname managed by the collection is required 